# What is Playlifin

It is a tool to convert Youtube Music playlist to Jellyfin playlist.

# How to use Playlifin ?

Playlifin takes a link to a Youtube playlist, get all the song in it and try to find them on your jellyfin. If it can find your songs, it will create a playlist named after the Youtube playlist and add all relevant songs in it.

To use it correctly, you need to follow these steps:


1. Open the App from your application Launcher

At the first launch you will need to configure your Jellyfin settings in the Preferences of the applications

Format required:

```
Jellyfin URL: Base URL without any trailing slashes
    ex: https://jellyfin.domain.org

Jellyfin API Key: Your API key

Jellyfin Username: The username who will own the playlists.
```

2. Copy the Youtube URL of the playlist, this should be in one of the following format:

Format required:

```url
    https://www.youtube.com/playlist?list=PLIXMFlnm0e9QcZafQRpyw8WwTC9LvEchu
    https://www.youtube.com/watch?v=JW7FUXBFKj4&list=PLIXMFlnm0e9QcZafQRpyw8WwTC9LvEchu&index=3

    OR, just the Playlist ID:
    PLIXMFlnm0e9QcZafQRpyw8WwTC9LvEchu
```

3. Paste the link in the main field of the Application and then click the Synchronize button

4. Wait for the software to import everything, once it's done, it will show you a notification and a popup.

# Videos

Playlifin can also work with videos downloaded from Youtube. For this to work you need to change the setting "Media Type" in the Preference of the application and select what you want Playlifin to search on the server.

**Warning: Activating both can lead to more false positives**