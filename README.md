# Playlifin

## What is Playlifin

It is a tool to convert Youtube Music playlist to Jellyfin playlist.

This program get all the song titles in your Youtube Playlist, tries its best to find the same song on Jellyfin and add it in the new playlist.

This is useful when you sort music by albums, genre or artists but still want to keep playlists

## Requierments

+ Python 3.11+

## How to use

[Link to the documentation](./docs/README.md)

## How to build Flatpak

```shell
flatpak-builder --user --install --force-clean build-dir net.krafting.Playlifin.yml && flatpak run net.krafting.Playlifin
```

## Screenshots

![Program's main window](screenshots/main_window.png)

![Program's main window with logs](screenshots/main_window_log.png)

## Generate releases:

Add content in NEWS, then launch:

```shell
appstreamcli news-to-metainfo ./NEWS ./net.krafting.Playlifin.metainfo.xml
```

And then update the version in the about page

We then update the version in the manifest locally, push everything and create a new release in gitlab and then update the version in the flathub repo.