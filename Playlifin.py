#!/usr/bin/python3
import json
import re
from datetime import datetime
import requests
import logging as log
import gi
import sys, os
import webbrowser
import yt_dlp
import threading
from urllib.parse import quote

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, GLib

# Disable Warning if using self signed cert:
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Create config folder
config_folder = str(GLib.get_user_config_dir()) + "/Playlifin"
if not os.path.exists(config_folder):
    os.makedirs(config_folder)
# Create the folder where songs not found will be stored
config_folder_not_found = str(GLib.get_user_config_dir()) + "/Playlifin/NotFound"
if not os.path.exists(config_folder_not_found):
    os.makedirs(config_folder_not_found)

"""
Init a logger, to get log in a better way than print()
Also put logs in the config folder in a playlifin.log file
"""
logger = log.getLogger()
# logger.setLevel(log.DEBUG)
logger.setLevel(log.INFO)
formatter = log.Formatter('%(asctime)s | %(levelname)s | [Playlifin] %(message)s', '%d-%m-%Y %H:%M:%S')
# Log inside the terminal
stdout_handler = log.StreamHandler(sys.stdout)
stdout_handler.setFormatter(formatter)
logger.addHandler(stdout_handler)
# Log inside a file too
file_handler = log.FileHandler(config_folder + "/playlifin.log")
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


class Playlifin(Adw.Application):
    def __init__(self):
        super().__init__(application_id="net.krafting.Playlifin")
        self.connect("activate", self.on_activate)
        
    def on_activate(self, app):
        self.config_folder = str(GLib.get_user_config_dir()) + "/Playlifin"
        self.config_folder_not_found = str(GLib.get_user_config_dir()) + "/Playlifin/NotFound"
        self.verifySSL = True

        # Initialize some variables for use later
        self.config = {}
        self.max_log_lines_default = 40

        # Set default settings
        self.window = Adw.ApplicationWindow(application=app)
        self.window.set_default_size(850, 600)
        self.window.set_property('height-request', 200)
        self.window.set_property('width-request', 300)
        self.window.set_title("Playlifin")
        self.window.connect("destroy", self.destroyed_window)
        self.window.connect("close-request", self.destroyed_window)
        GLib.set_application_name("Playlifin")
        self.window.present()

        #######################
        # MAIN PAGE
        ######################
        self.Main_boxMain = Adw.ToolbarView()

        # All page is a Toast Overlay to display burnt toast
        self.Main_Toast = Adw.ToastOverlay()
        self.Main_Toast.set_child(self.Main_boxMain)
        self.window.set_content(self.Main_Toast)  # Horizontal box to window

        self.Main_page = Adw.StatusPage()
        self.Main_page.set_title('Playlifin')
        self.Main_page.set_description('Sync Youtube playlists with your Jellyfin server.')
        self.Main_boxMain.set_content(self.Main_page)

        self.Main_clamp = Adw.Clamp()
        self.Main_page.set_child(self.Main_clamp)

        self.Main_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.Main_clamp.set_child(self.Main_box2)

        self.Main_listbox = Gtk.ListBox()
        self.Main_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.Main_listbox.add_css_class("boxed-list")
        self.Main_box2.append(self.Main_listbox)

        self.Main_Spinner = Gtk.Spinner()
        self.Main_Spinner.set_spinning(False)

        self.Main_Entryrow = Adw.EntryRow()
        self.Main_Entryrow.set_title('Youtube Playlist Link or ID')
        self.Main_Entryrow.add_suffix(self.Main_Spinner)
        self.Main_listbox.append(self.Main_Entryrow)

        self.sync_button = Gtk.Button(label="Synchronize")
        self.sync_button.add_css_class("pill")
        self.sync_button.add_css_class("suggested-action")
        self.sync_button.set_halign(Gtk.Align.CENTER)
        self.sync_button.connect("clicked", self.check_sync_playlist)
        self.sync_button.set_margin_bottom(15)
        self.sync_button.set_margin_top(15)
        self.Main_box2.append(self.sync_button)

        #######################
        # MAIN PAGE HEADERBAR
        #######################
        self.header = Adw.HeaderBar()
        self.Main_boxMain.add_top_bar(self.header)

        # Create a popover
        self.menu = Gio.Menu.new()
        self.popover = Gtk.PopoverMenu()  # Create a new popover menu
        self.popover.set_menu_model(self.menu)

        # Create a menu button
        self.hamburger = Gtk.MenuButton()
        self.hamburger.set_popover(self.popover)
        self.hamburger.set_icon_name("open-menu-symbolic")  # Give it a nice icon

        # Add menu button to the header bar (at the end)
        self.header.pack_end(self.hamburger)

        # Add a link to the documentation
        action_docs = Gio.SimpleAction.new("docs", None)
        action_docs.connect("activate", self.open_browser, "https://gitlab.com/Krafting/playlifin-gtk/-/blob/main/docs/README.md")
        self.window.add_action(action_docs)
        self.menu.append("How to use ?", "win.docs")

        # Add shortcuts dialog
        action_preferences = Gio.SimpleAction.new("preferences", None)
        action_preferences.connect("activate", self.show_settings)
        self.window.add_action(action_preferences)
        self.menu.append("Preferences", "win.preferences")

        # Add an about dialog
        action_about = Gio.SimpleAction.new("about", None)
        action_about.connect("activate", self.show_about)
        self.window.add_action(action_about) 
        self.menu.append("About", "win.about")

        # evk = Gtk.EventControllerKey.new()
        # evk.connect("key-pressed", self.key_press)
        # self.add_controller(evk)

        #######################
        # SETTINGS DIALOG
        #######################
        self.Preferences_application_dialog = Adw.PreferencesWindow()
        self.Preferences_application_dialog.set_title("Preferences")
        self.Preferences_application_dialog.set_hide_on_close(True)
        self.Preferences_application_dialog.set_transient_for(self.window)
        self.Preferences_application_dialog.set_default_size(600, 460)

        # Premiere page des paramètres
        self.Preferences_PreferencePageSoftware = Adw.PreferencesPage()
        self.Preferences_PreferencePageSoftware.set_title('Software')
        self.Preferences_PreferencePageSoftware.set_icon_name('emblem-system-symbolic')
        self.Preferences_application_dialog.add(self.Preferences_PreferencePageSoftware)

        # 1er groupe de préférences
        self.Preferences_Group = Adw.PreferencesGroup()
        self.Preferences_Group.set_title('Jellyfin Settings')
        self.Preferences_Group.set_description('Settings of your Jellyfin server.')
        self.Preferences_PreferencePageSoftware.add(self.Preferences_Group)

        # Jellyfin URL
        self.Preferences_URL = Adw.EntryRow()
        self.Preferences_URL.set_title('Jellyfin URL')
        self.Preferences_Group.add(self.Preferences_URL)

        # Jellyfin API Key
        self.Preferences_API_Key = Adw.EntryRow()
        self.Preferences_API_Key.set_title('Jellyfin API Key')
        self.Preferences_Group.add(self.Preferences_API_Key)

        # Jellyfin Username
        self.Preferences_User_ID = Adw.EntryRow()
        self.Preferences_User_ID.set_title('Jellyfin Username')
        self.Preferences_Group.add(self.Preferences_User_ID)

        # Don't Verify SSL
        self.Preferences_SwitchSSL = Adw.SwitchRow()
        self.Preferences_SwitchSSL.set_title('Do not verify SSL Certificate')
        self.Preferences_SwitchSSL.set_subtitle('Do not validate SSL certificates of the Jellyfin server. Useful when using self-signed certificates on the server.')
        self.Preferences_Group.add(self.Preferences_SwitchSSL)

        # MediaType selection (From Audio to Video to Books)
        self.Preferences_MediaTypeString = Gtk.StringList()
        self.Preferences_MediaTypeString.append('Audio')
        self.Preferences_MediaTypeString.append('Video')
        self.Preferences_MediaTypeString.append('Audio & Video')

        self.Preferences_MediaType = Adw.ComboRow()
        self.Preferences_MediaType.set_title('Media Type')
        self.Preferences_MediaType.set_subtitle('Playlifin will only search on selected MediaType.')
        self.Preferences_MediaType.set_model(self.Preferences_MediaTypeString)
        self.Preferences_Group.add(self.Preferences_MediaType)

        self.Preferences_MediaType.connect("notify", self.save_settings)
        self.Preferences_URL.connect("notify", self.save_settings)
        self.Preferences_API_Key.connect("notify", self.save_settings)
        self.Preferences_User_ID.connect("notify", self.save_settings)
        self.Preferences_SwitchSSL.connect("notify", self.save_settings)

        # 2nd groupe de préférences pour séparer du reste
        self.Preferences_GroupConsole = Adw.PreferencesGroup()
        self.Preferences_GroupConsole.set_title('Console Output Settings')
        self.Preferences_GroupConsole.set_description('Options are applied automatically.')

        self.load_settings()

    """
    Function called when the main window is destroyed/closed
    Used to prevent the thread to keep running in the background.
    """
    def destroyed_window(self, signal):
        # We kill the process
        self.IsThreadCancelled = True

    """
    Function to check if we can start the sync!
    Check if the server is reacheable and if configuration is set correctly
    """
    def check_sync_playlist(self, button):
        if "jf_url" in self.config:
            if self.config['jf_url'] == "" or self.config['jf_api_key'] == "" or self.config['jf_user_id'] == "":
                self.create_dialog("Bad configuration", "Please ensure your Jellyfin server is configured correctly in the settings", "settings")
                return False

            ping_jellyfin = self.ping_jellyfin()
            if ping_jellyfin != 200:
                self.create_dialog("Bad configuration", "Please ensure your Jellyfin server is configured correctly in the settings", "settings")
                return False
            
            value_playlist = self.parse_youtube_url(self.Main_Entryrow.get_text())
            if value_playlist == False:
                self.create_dialog("Bad playlist link", "Please enter a correct value")
                return False
            self.Main_Entryrow.set_sensitive(False)
            self.sync_button.set_sensitive(False)
            self.Main_Spinner.set_spinning(True)

            # Create a cancel button
            self.sync_buttonCancel = Gtk.Button(label="Cancel")
            self.sync_buttonCancel.add_css_class("pill")
            self.sync_buttonCancel.set_halign(Gtk.Align.CENTER)
            self.sync_buttonCancel.set_margin_bottom(15)
            self.sync_buttonCancel.set_margin_top(15)
            self.sync_buttonCancel.connect("clicked", self.cancel_everything)
            self.Main_box2.remove(self.sync_button)
            self.Main_box2.append(self.sync_buttonCancel)

            self.create_dialog("Importing...", "Please wait while the software import the playlist in Jellyfin...\n This can take a while depending of the size of your playlist...")
            # cmd = "ls"

            # Remove old buttons (Open in youtube & jellyfin)
            if hasattr(self, 'Main_ActionRowButton'):
                self.Main_ActionRowButton.remove(self.Main_LinkButton)
                self.Main_listbox.remove(self.Main_ActionRowButton)
                self.Main_listbox.remove(self.Main_ActionRowStatus)

            # Remove old row for Not Found songs
            if hasattr(self, 'Main_expenderrowNotFound'):
                self.Main_listbox.remove(self.Main_expenderrowNotFound)

            self.Main_LinkButton = Gtk.Button()
            self.Main_LinkButton.set_label("Open in Youtube")
            self.Main_LinkButton.set_valign(Gtk.Align.CENTER)
            self.Main_LinkButton.connect("clicked", self.open_playlist, value_playlist)

            self.Main_ActionRowButton = Adw.ActionRow()
            self.Main_ActionRowButton.add_prefix(self.Main_LinkButton)
            self.Main_listbox.append(self.Main_ActionRowButton)

            self.Main_ActionRowStatus = Adw.ActionRow()
            self.StatusText = Gtk.Label(label="Waiting for Youtube...")
            self.StatusText.add_css_class("subtitle")
            self.StatusText.add_css_class("center")
            self.StatusText.add_css_class("center-text")
            self.StatusText.set_halign(Gtk.Align.CENTER)
            self.StatusText.set_margin_top(4)
            self.StatusText.set_margin_bottom(4)
            self.StatusText.set_hexpand(True)
            self.Main_ActionRowStatus.set_child(self.StatusText)
            self.Main_listbox.append(self.Main_ActionRowStatus)
            
            self.IsThreadCancelled = False
            self.sync_playlist(value_playlist)
        else:
            self.create_dialog("Bad configuration", "Please ensure your Jellyfin server is configured correctly in the settings", "settings")
            return False
    """
    Function used to call the function to import a the content of a generated file to the Jellyfin server
    """
    def sync_playlist(self, value_playlist):
        # Start the asynchronous request in a separate thread
        self.MainThread = threading.Thread(target=self.sync_playlist_async)
        self.MainThread.start()

    """
    Main function of this file to sync a playlist to Jellyfin.
    """
    def sync_playlist_async(self):
        value_playlist = self.Main_Entryrow.get_text()

        log.info('Importing Link: ' + str(value_playlist))
        extract_stuff = self.extract_playlist_info(value_playlist)

        if extract_stuff[0] == False:
            log.info('Error: Error while importing: ' + type(extract_stuff[1]).__name__ + ": " + str(extract_stuff[1]))
            self.error_on_import()
            return

        # Create the playlist and show the button to access the playlist
        all_songs_name, playlist_title, all_video_url = [extract_stuff[0], extract_stuff[1], extract_stuff[2]]
        self.current_playlist_jellyfin_id = self.create_playlist(playlist_title)

        if self.current_playlist_jellyfin_id == False:
            return False

        playlist_created_id_url = str(self.config["jf_url"]) + "/web/index.html#!/details?id=" + str(self.current_playlist_jellyfin_id)
        self.Main_LinkButton = Gtk.Button()
        self.Main_LinkButton.set_label("Open in Jellyfin")
        self.Main_LinkButton.add_css_class("suggested-action")
        self.Main_LinkButton.set_valign(Gtk.Align.CENTER)
        self.Main_LinkButton.connect("clicked", self.open_playlist, playlist_created_id_url)
        self.Main_ActionRowButton.add_suffix(self.Main_LinkButton)

        log.info('Importing Playlist Name: ' + str(playlist_title))
        not_found_array = ""
        i = 1
        totalLen = len(all_songs_name)

        # By using GLib.idle_add, we're ensuring that the GUI operations (in this case, creating the dialog) 
        # happen in the main thread, thus avoiding potential threading issues with GTK.
        def update_status_bar(i, total, tries):
            self.StatusText.set_text("Importing " + str(i) + " of " + str(total) + ("." * tries))

        for video_title in all_songs_name:
            og_video_title = video_title
            found = False
            video_url_searching = all_video_url[video_title]
            tries = 0
            while found is False and tries <= 6:
                # If we cancel everything, we just return to stop all execution
                if self.IsThreadCancelled:
                    log.info("Thread killed")
                    return
                # Show current progress with a small animation with the dots
                GLib.idle_add(update_status_bar, i, totalLen, (tries%3+1))

                tries = tries + 1

                if tries > 1:
                    # Remove weird emojis sometime
                    if " ◉ " in video_title:
                        video_title = video_title.split(' ◉ ')
                        video_title = video_title[1]
                        found = self.search_jellyfin(video_title)[1]
                        
                    # Remove slash
                    # Replace with the YT-DLP slash too
                    if "/" in video_title:
                        # video_title = video_title.split('/')
                        video_title = re.sub('/','⧸',video_title)
                        found = self.search_jellyfin(video_title)[1]

                    # Yt-dlp download videos and put weird chars instead of the right ones (when you download videos)...
                    # So we need to try the same thing, but searching with yt-dlp doesn't replace chars..... this is so dumb.....
                    # Anyway, here I am, creating a workaround...
                    if ":" in video_title:
                        video_title = re.sub(':','：',video_title)
                        found = self.search_jellyfin(video_title)[1]
                    if "|" in video_title:
                        video_title = re.sub('|','｜',video_title)
                        found = self.search_jellyfin(video_title)[1]
                    if "?" in video_title:
                        video_title = re.sub(r'\?','？',video_title)
                        found = self.search_jellyfin(video_title)[1]
                    if "*" in video_title:
                        video_title = re.sub(r'\*','＊',video_title)
                        found = self.search_jellyfin(video_title)[1]
                    if '"' in video_title:
                        video_title = re.sub('"','＂',video_title)
                        found = self.search_jellyfin(video_title)[1]

                    # Jellyfin can't search pipes
                    if "|" in video_title:
                        video_title = video_title.split('|')
                        video_title = video_title[0]
                        found = self.search_jellyfin(video_title)[1]

                    # Split  -  so we can search only the song names
                    if " - " in video_title:
                        video_title = video_title.split(' - ')
                        video_title = video_title[1]
                        found = self.search_jellyfin(video_title)[1]

                    if ":" in video_title:
                        video_title = video_title.split(':')
                        video_title = video_title[1]
                        found = self.search_jellyfin(video_title)[1]

                        
                    if "]" in video_title:
                        video_title = re.sub(r'\[.*\]','',video_title)
                        found = self.search_jellyfin(video_title)[1]

                    if ")" in video_title:
                        video_title = re.sub(r'\(.*\)','',video_title)
                        found = self.search_jellyfin(video_title)[1]

                    if "#" in video_title:
                        video_title = video_title.replace('#', '')
                        found = self.search_jellyfin(video_title)[1]

                else:

                    found = self.search_jellyfin(video_title)[1]
                    
                search_ids = self.search_jellyfin(video_title)[0]

                if(video_title == ""):
                    log.info("Nothing Found. Empty name returned for: " + og_video_title + "   URL: " + video_url_searching)
                    continue
            
            # Number of items, to show progress in the App
            i = i + 1
            
            # Matched video title (or not):
            log.info("Importing: " + str(video_title))
            if search_ids != []:
                # Get first result ID
                song_id = search_ids['ItemId']            
                # Add songs to playlist
                self.add_song_to_playlist(self.current_playlist_jellyfin_id, song_id)
            else: 
                log.info("Nothing found for: " + str(og_video_title) + "   URL: " + video_url_searching)
                not_found_array = not_found_array + "$$$" + str(og_video_title) + "-_-_-" + str(video_url_searching)
                continue

        # Finished import, put the buttons in the default position and show a Popup
        log.info('FINISHED: Playlist imported successfully')
        self.Main_Entryrow.set_sensitive(True)
        self.sync_button.set_sensitive(True)
        self.Main_Spinner.set_spinning(False)
        self.create_dialog("Importing Complete", "Importation was successful, you can now listen to your music in the playlist: " + str(playlist_title))
        self.StatusText.set_text("Imported " + str(playlist_title))
        # We put the button back where they are from!
        self.Main_box2.remove(self.sync_buttonCancel)
        self.Main_box2.append(self.sync_button)

        # If at the end we didn't find some songs, then we show them to the user
        if len(not_found_array.split("$$$")) >= 1:
            log.info('Songs not found: ' + str(not_found_array))
            full_text = "Songs that could not be found on Jellyfin and were not added to the playlist:\n"
            for title in not_found_array.split("$$$"):
                if title == "":
                    continue
                title_not_found = title.split("-_-_-")[0]
                url_not_found = title.split("-_-_-")[1]
                full_text = full_text + title_not_found + " (URL: " + str(url_not_found) + ")" + "\n"
            # Save songs not found in file for better usage.
            current_date = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
            fileNotFound = open(self.config_folder_not_found + '/' + str(current_date) + '_songs_not_found.txt', 'w')
            fileNotFound.write(full_text)
            fileNotFound.close()

            # Remove old row
            if hasattr(self, 'Main_expenderrowNotFound'):
                self.Main_actionrownotFound.remove(self.Main_ScrollNotFound)
                self.Main_listbox.remove(self.Main_expenderrowNotFound)
                
            self.Main_expenderrowNotFound = Adw.ExpanderRow()
            self.Main_expenderrowNotFound.set_title('Songs not found on Jellyfin')
            self.Main_listbox.append(self.Main_expenderrowNotFound)

            self.Main_actionrownotFound = Adw.ActionRow()
            self.Main_actionrownotFound.add_css_class("monospace")
            self.Main_actionrownotFound.set_title_selectable(True)
            self.Main_actionrownotFound.set_use_markup(False)
            self.Main_actionrownotFound.set_title(str(full_text))
            
            self.Main_ScrollNotFound = Gtk.ScrolledWindow()
            self.Main_ScrollNotFound.set_min_content_height(400)
            self.Main_ScrollNotFound.set_max_content_height(500)
            self.Main_ScrollNotFound.set_propagate_natural_height(True)
            self.Main_ScrollNotFound.set_child(self.Main_actionrownotFound)
            self.Main_expenderrowNotFound.add_row(self.Main_ScrollNotFound)
        

    """
    Function to get a user ID from the Username
    """
    def get_user_id(self):
        api_search_users = self.config['jf_url'] + "/Users" + "?api_key=" + self.config['jf_api_key']
        response = requests.get(api_search_users, verify=self.verifySSL)
        allUsers = response.json()
        for user in allUsers:
            if user["Name"].lower() == self.config['jf_user_id'].lower():
                return user["Id"]
        return False

    """
    Function to create the playlist with a given name on Jellyfin
    """
    def create_playlist(self, playlist_name):
        # Create Playlist & Get ID
        api_create_pl = self.config['jf_url'] + "/Playlists" + "?api_key=" + self.config['jf_api_key']

        # We get the MediaType setting
        if 'mediatype_name' in self.config:
            if self.config['mediatype_name'] == "Audio & Video":
                mediaType = "Mixed"
            else:
                mediaType = self.config['mediatype_name']
        else:
            mediaType = "Audio"

        userId = self.get_user_id()
        if(userId == False):
            log.error('Error: Jellyfin username is incorrect.')
            return False
        data = {
            "Name": playlist_name,
            "Ids": [],
            "UserId": userId,
            "MediaType": mediaType,
        }
        response = requests.post(api_create_pl, json=data, verify=self.verifySSL)
        self.current_playlist_jellyfin_id = response.json()["Id"]
        log.info("Created playlist with id " + self.current_playlist_jellyfin_id)
        return self.current_playlist_jellyfin_id

    """
    Function to search jellyfin with a specific search term (here, wideo titles)
    """
    def search_jellyfin(self, video_title):
        # We get the MediaType setting
        if 'mediatype_name' in self.config:
            if self.config['mediatype_name'] == "Audio & Video":
                mediaType = "Audio,Video"
            else:
                mediaType = self.config['mediatype_name']
        else:
            mediaType = "Audio"
        
        api_search_jf = self.config['jf_url'] + "/Search/Hints" + "?api_key=" + self.config['jf_api_key'] + "&mediaTypes=" + mediaType + "&searchTerm=" +  quote(video_title)
        # log.debug(api_search_jf)
        if(video_title == ""):
            return [], False
        response = requests.get(api_search_jf, verify=self.verifySSL)
        log.debug(api_search_jf)
        if response.status_code != 200:
            log.error('Something went wrong for the song: ' + video_title)
            return [], False
        # log.info(response)
        # print(response)
        # print(response.json())
        search_ids = response.json()["SearchHints"]
        if search_ids == []:
            found = False
            send_search_id = []
        else:
            found = True
            # Check if there is an exact match from all the results (just to be sure...)
            for search_id in search_ids:
                # if()
                log.debug("checking : " + search_id["Name"])
                log.debug("for : " + video_title)
                if search_id["Name"] == video_title:
                    is_there_exact_match = True
                    send_search_id = search_id
                else:
                    is_there_exact_match = False
                    continue

            if not is_there_exact_match:
                send_search_id = search_ids[0]

        return send_search_id, found

    """
    Function to add a song to a playlist on Jellyfin
    """
    def add_song_to_playlist(self, current_playlist_jellyfin_id, song_id):
        api_add_to_pl = self.config['jf_url'] + "/Playlists/" + self.current_playlist_jellyfin_id + "/Items?Ids=" + song_id + "&api_key=" + self.config['jf_api_key']
        response = requests.post(api_add_to_pl, verify=self.verifySSL)

    """
    Function to extract every Youtube video name in a playlist
    """
    def extract_playlist_info(self, playlist):
        # YT-DLP options and fetch list of all video names
        log.info('Searching names of song in playlist...')
        try:
            ydl_opts = {
                'extract_flat': 'in_playlist',
                'outtmpl': '%(title)s.%(ext)s'
            }
            yt_dlp_custom = yt_dlp.YoutubeDL(ydl_opts)
            info_dict = yt_dlp_custom.extract_info(playlist, download=False)
        
            og_playlist_title = info_dict.get('title', None)
            # We add the names to an array so it is easier to work with
            array_playlist_name = []
            array_playlist_url = {}
            for entry in info_dict["entries"]:
                if entry['title'] == "[Deleted video]":
                    log.info("A video was deleted. Skipping. URL: " + str(entry['url']))
                    continue
                if entry['title'] == "[Private video]":
                    log.info("A video was made private. Skipping. URL: " + str(entry['url']))
                    continue
                video_title = entry.get('title', None)
                video_url = entry.get('url', None)
                array_playlist_url[video_title] = str(video_url)
                array_playlist_name.append(video_title)
            return array_playlist_name, og_playlist_title, array_playlist_url
        except Exception as err:
            self.error_on_import()
            return False, err

    """
    Function to cancel the loading of the playlist.
    """
    def cancel_everything(self, output) -> None:
        # We kill the process
        self.IsThreadCancelled = True

        self.Main_Entryrow.set_sensitive(True)
        self.sync_button.set_sensitive(True)
        self.Main_Spinner.set_spinning(False)
        
        # We put the button back where they are from!
        self.Main_box2.remove(self.sync_buttonCancel)
        self.Main_box2.append(self.sync_button)
        self.StatusText.set_text("Cancelled by the user.")


    """
    Function to put everything back in place in case of an error during the Import
    """
    def error_on_import(self) -> None:
        # We kill the process
        self.IsThreadCancelled = True
        self.Main_Entryrow.set_sensitive(True)
        self.sync_button.set_sensitive(True)
        self.Main_Spinner.set_spinning(False)
        self.create_dialog("Error while importing", "An exception has happened while trying to import your playlist. Please check the logs.")
        self.StatusText.set_text("An error occured.")
        # We put the button back where they are from!
        self.Main_box2.remove(self.sync_buttonCancel)
        self.Main_box2.append(self.sync_button)

    """
    Function to open a URL in the browser
    """
    def open_playlist(self, button, playlist_link) -> None:
        webbrowser.open(str(playlist_link))

    """
    Function to get the ID of the playlist with whatever the user gives us.
    Link Implemented:
        https://www.youtube.com/playlist?list=PLIXMFlnm0e9QcZafQRpyw8WwTC9LvEchu
        https://www.youtube.com/watch?v=JW7FUXBFKj4&list=PLIXMFlnm0e9QcZafQRpyw8WwTC9LvEchu&index=3
        PLIXMFlnm0e9QcZafQRpyw8WwTC9LvEchu
    """
    def parse_youtube_url(self, playlist_yt_id):
        if playlist_yt_id == "":
            log.info('No value entered...')
            return False
        if "http" not in playlist_yt_id:
            log.info('No http, assuming Playlist ID.. adding link')
            return "https://www.youtube.com/playlist?list=" + str(playlist_yt_id)
        if "http" in playlist_yt_id:
            return playlist_yt_id

        return False

    """
    Function to see if the server is reacheable with its API key
    """
    def ping_jellyfin(self):
        try:
            ping_jellyfin = self.config['jf_url'] + "/System/Info" + "?api_key=" + self.config['jf_api_key']
            response = requests.get(ping_jellyfin, verify=self.verifySSL)
            return response.status_code
        except:
            return 1024

    """
    Function to easily create Dialog on demand.
    Typed can be use to replace buttons and actions of said button
    """
    def create_dialog(self, title, message, typed = "normal"):
        # By using GLib.idle_add, we're ensuring that the GUI operations (in this case, creating the dialog) 
        # happen in the main thread, thus avoiding potential threading issues with GTK.
        def create_dialog_callback():
            # Remove previous dialogs
            if hasattr(self, 'Main_dialog'):
                self.Main_dialog.destroy()
            self.Main_dialog = Adw.MessageDialog()
            self.Main_dialog.set_modal(True)
            self.Main_dialog.set_heading(str(title))
            self.Main_dialog.set_close_response("close-dialog")
            self.Main_dialog.set_transient_for(self.window)
            self.Main_dialog.set_body(str(message))
            self.Main_dialog.set_body_use_markup(False)
            
            if typed == "settings":
                self.Main_dialog.add_response('close-dialog', 'Close')
                self.Main_dialog.add_response('open-settings', 'Open Settings')
                self.Main_dialog.set_response_appearance('open-settings', Adw.ResponseAppearance.SUGGESTED)
            else:
                self.Main_dialog.add_response('close-dialog', 'Okay')

            self.Main_dialog.choose(callback=self.on_response_selected)

        # Schedule the creation of the dialog in the main event loop
        GLib.idle_add(create_dialog_callback)

    """
    Function to handle response of Dialogs
    """
    def on_response_selected(self, response, task):
        response = response.choose_finish(task)
        if response == "open-settings":
            self.show_settings()

    """
    Function to save settings data to the self.config variable
    Then, write a settings.json file to the config folder of the app
    """
    def save_settings(self, action, param) -> None:
        self.config['jf_url'] = self.Preferences_URL.get_text()
        self.config['jf_api_key'] = self.Preferences_API_Key.get_text()
        self.config['jf_user_id'] = self.Preferences_User_ID.get_text()
        self.config['verify_ssl'] = self.Preferences_SwitchSSL.get_active()
        if self.config['verify_ssl'] == 1:
            self.verifySSL = False
        else:
            self.verifySSL = True
        self.config['mediatype_name'] = self.Preferences_MediaTypeString.get_string(self.Preferences_MediaType.get_selected())
        self.config['mediatype_id'] = self.Preferences_MediaType.get_selected()
        json.dump( self.config, open( self.config_folder + "/settings.json", 'w' ) )

    """
    Function to load the settings data at startup of the application
    This is a bit hacky, when you load a value, it send the signal to save settings and overwriting 
    everything else, so we store the config in variable, change the values we want and then load it again
    """
    def load_settings(self) -> None:
        if not os.path.exists( self.config_folder + "/settings.json" ): 
            return
        try:
            loading_config = json.load( open( self.config_folder + "/settings.json" ) )
            self.Preferences_URL.set_text(loading_config['jf_url'])
            self.Preferences_API_Key.set_text(loading_config['jf_api_key'])
            self.Preferences_User_ID.set_text(loading_config['jf_user_id'])
            self.Preferences_SwitchSSL.set_active(loading_config['verify_ssl'])
            if self.config['verify_ssl'] == 1:
                self.verifySSL = False
            else:
                self.verifySSL = True
            self.Preferences_MediaType.set_selected(loading_config['mediatype_id'])
            self.config = loading_config
        except:
            log.info("Error in config file... Using default values")

    """
    Function to open link in the browser
    """
    def open_browser(self, button, action, link) -> None:
        webbrowser.open(str(link))

    """
    Function to create and show the AboutWindow
    """
    def show_about(self, action, param) -> None:
        self.about = Adw.AboutWindow(transient_for=self.get_active_window())
        self.about.set_transient_for(self.window)
        self.about.set_modal(self.window)
        self.about.set_application_icon('net.krafting.Playlifin')
        self.about.set_application_name("Playlifin")
        self.about.set_developers(["Krafting"])
        self.about.set_copyright("Copyright 2023 Krafting")
        self.about.set_license_type(Gtk.License.GPL_3_0)
        self.about.add_credit_section("Icon by", ["Your Name Here, contact me!"])
        self.about.set_version("2.1")
        self.about.set_website("https://gitlab.com/Krafting/playlifin-gtk/")
        self.about.set_issue_url("https://gitlab.com/Krafting/playlifin-gtk/-/issues")
        self.about.set_icon_name("net.krafting.Playlifin")
        self.about.present()

    """
    Function to present (show) the Settings/Preferences window
    """
    def show_settings(self, action = None, param = None) -> None:
        self.Preferences_application_dialog.present()


def main():
    app = Playlifin()
    app.run(sys.argv)

if __name__ == "__main__":
    main()